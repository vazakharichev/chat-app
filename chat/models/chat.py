from .base import db


class Chat(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    unit_id = db.Column(db.Integer, nullable=False)
    student_id = db.Column(db.Integer, nullable=False)

    messages = db.relationship('Message', back_populates='chat')

    __tablename__ = 'chats'
